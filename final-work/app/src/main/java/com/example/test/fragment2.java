package com.example.test;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class fragment2 extends Fragment {

    private RecyclerView recyclerView;
    private List<HashMap<String, Object>> list;
    private MyDAO myDAO;
    private Context context;
    private Myadapter myadapter;

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myDAO = new MyDAO(getContext());
        myDAO.MyconnectionDB();

        list = myDAO.MyqueryAll("select DISTINCT * from person");

        View view = inflater.inflate(R.layout.middle2, container, false);
        context = view.getContext();
        recyclerView = view.findViewById(R.id.recycleview1);

        myadapter = new Myadapter(context, new ArrayList<>(list));

        myadapter.setOnItemClickListener(new Myadapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                HashMap<String, Object> item = list.get(position);
                Log.d("hhdsada", "Starting MainActivity1...");
                String name = (String) item.get("name");
                Intent intent = new Intent(getContext(), MainActivity1.class);
                intent.putExtra("data", name);
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(myadapter);

        LinearLayoutManager manager = new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);

        return view;
    }
}