package com.example.test;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

public class MyDAO {

    static SQLiteDatabase database;

    MyOpenHelper openHelper;

    Context mycontext;

    public MyDAO(Context context){
        mycontext=context;
    }
    public void MyconnectionDB(){
        openHelper=new MyOpenHelper(mycontext,"ftDB",null,1);
        database=openHelper.getWritableDatabase();
        openHelper.onCreate(database);
    }//连接到数据库

    public void Myinsert(String name,int age){
        ContentValues values=new ContentValues();
        values.put("name",name);
        values.put("age",age);
        database.insert("person",null,values);

    }


    public void Mydelete(){
        database.delete("person", null, null);
    }



    public ArrayList<HashMap<String, Object>> MyqueryAll(String str){
        Cursor cursor=database.rawQuery("SELECT id, name, age FROM person GROUP BY name", null);

        StringBuilder stringBuilder=new StringBuilder();//实现表中一行所有信息的整合


        cursor.moveToFirst();
        ArrayList<HashMap<String,Object>> list1=new ArrayList<>();
        while (!cursor.isAfterLast()){

            @SuppressLint("Range") int id=cursor.getInt(cursor.getColumnIndex("id"));//当cursor想获得int值时，该值要从0开始，但getColumnIndex起步从-1开始，所以会报错，要加一个标签忽略掉它
            @SuppressLint("Range") String name=cursor.getString(cursor.getColumnIndex("name"));
            HashMap<String,Object> map=new HashMap<>();
            map.put("id",id);
            map.put("name",name);
            list1.add(map);
            cursor.moveToNext();
        };
        cursor.close();
        return list1;}

}
