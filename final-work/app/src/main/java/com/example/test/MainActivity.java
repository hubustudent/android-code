package com.example.test;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;



public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    LinearLayout layout1,layout2,layout3,layout4,layout5;
    Fragment fragment1,fragment2,fragment3,fragment4;
    FragmentManager manager;

    MyDAO myDAO = new MyDAO(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        myDAO.MyconnectionDB();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //列表
        layout1 = findViewById(R.id.layout1);
        layout2 = findViewById(R.id.layout2);
        layout3 = findViewById(R.id.layout3);
        layout4 = findViewById(R.id.layout4);
        layout5 = findViewById(R.id.layout5);

        fragment1 = new fragment1();
        fragment2 = new fragment2();
        fragment3 = new fragment3();
        fragment4 = new fragment4();

        manager=getSupportFragmentManager();
        FragmentTransaction fg=manager.beginTransaction();

        initialfragment();


        layout1.setOnClickListener(this);
        layout2.setOnClickListener(this);
        layout3.setOnClickListener(this);
        layout4.setOnClickListener(this);
        layout5.setOnClickListener(this);

    }


    public void initialfragment(){
        FragmentTransaction fg= manager.beginTransaction()
                .add(androidx.appcompat.R.id.content,fragment1)
                .add(androidx.appcompat.R.id.content,fragment2)
                .add(androidx.appcompat.R.id.content,fragment3)
                .add(androidx.appcompat.R.id.content,fragment4)
                .hide(fragment1)
                .hide(fragment2)
                .hide(fragment3)
                .hide(fragment4)
                .show(fragment1);
        fg.commit();
    }
    private void show(int i) {
        FragmentTransaction transaction = manager.beginTransaction();
        Hide(transaction);
        switch (i){
            case 1:transaction.show(fragment1);
                break;
            case 2:transaction.show(fragment2);
                break;
            case 3:transaction.show(fragment3);
                break;
            case 4:transaction.show(fragment4);
                break;
            default:break;
        }
        transaction.commit();
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.layout1) show(1);
        if(view.getId()==R.id.layout2) show(2);
        if(view.getId()==R.id.layout3) show(3);
        if(view.getId()==R.id.layout4) show(4);
        if(view.getId()==R.id.layout5) {
            // 跳转到新的Activity
            Intent intent = new Intent(MainActivity.this, InputActivity.class);
            startActivity(intent);
        }
    }



    private void Hide(FragmentTransaction fg) {
        fg.hide(fragment1)
                .hide(fragment2)
                .hide(fragment3)
                .hide(fragment4);
    }

}