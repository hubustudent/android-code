package com.example.test;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class MyService1 extends Service {

    static MediaPlayer player;
    int currentMusicIndex = 0;
    int [] musics={
            R.raw.music1,
            R.raw.music2,
            R.raw.music3,
            R.raw.music4
    };
    public MyService1() {

    }
    @Override
    public void onCreate(){
        super.onCreate();
        Log.d("xr","MyService1:onCreate...");
        //player=MediaPlayer.create(this,musics[0]);
        //player=MediaPlayer.create(this,R.raw.music1);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("xr","MyService1:onStart...");
        int musicIndex = intent.getIntExtra("musicIndex", 0); // 获取传递的参数，默认为0
        playMusic(musicIndex);
        return super.onStartCommand(intent, flags, startId);
    }

    public void playMusic(int index) {
        if (player != null && player.isPlaying()) {
            player.stop();
        }//播放音乐时如果当前还有音乐正在播放就先暂停该音乐再播放下一首
        player = MediaPlayer.create(this, musics[index]);
        player.start();
        currentMusicIndex = index;
    }


    @Override
    public void onDestroy() {
        player.stop();
        Log.d("xr","MyService1:onDestroy...");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
