package com.example.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MusicActivity extends AppCompatActivity {
    private ImageView imageView01, imageView02, imageView03, imageView04;
    private Button button01, button02, button03;

    Intent intent;
    int index;




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);

        imageView01 = findViewById(R.id.imageView01);
        imageView02 = findViewById(R.id.imageView02);
        imageView03 = findViewById(R.id.imageView03);
        imageView04 = findViewById(R.id.imageView04);
        button01 = findViewById(R.id.button01);
        button02 = findViewById(R.id.button02);
        button03 = findViewById(R.id.button03);

        imageView01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MusicActivity.this, MyService1.class);
                index=0;
                intent.putExtra("musicIndex", index);
                startService(intent);
            }
        });

        imageView02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MusicActivity.this, MyService1.class);
                index=1;
                intent.putExtra("musicIndex", index);
                startService(intent);
            }
        });

        imageView03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MusicActivity.this, MyService1.class);
                index=2;
                intent.putExtra("musicIndex", index);
                startService(intent);
            }
        });

        imageView04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MusicActivity.this, MyService1.class);
                index=3;
                intent.putExtra("musicIndex", index);
                startService(intent);
            }
        });


        button01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index== 0) {
                    index=3;
                }
                else{
                    index--;
                }
                intent = new Intent(MusicActivity.this, MyService1.class);
                intent.putExtra("musicIndex", index);
                startService(intent);
            }
        });

        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopService(intent);
            }
        });

        button03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (index== 3) {
                    index=0;
                }
                else{
                    index++;
                }
                intent = new Intent(MusicActivity.this, MyService1.class);
                intent.putExtra("musicIndex", index);
                startService(intent);
            }
        });


    }
}


