package com.example.test;
import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.content.Context;
import android.view.LayoutInflater;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class Myadapter extends RecyclerView.Adapter<Myadapter.Myholder> {

    Context context1;
    ArrayList<HashMap<String,Object>> list1;
    OnItemClickListener listener;

    public Myadapter(Context context, ArrayList<HashMap<String, Object>> list){
        context1=context;
        list1=list;
        mylistener = listener;

    }

    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context1).inflate(R.layout.item,parent,false);
        Myholder holder=new Myholder(view);
        return holder;
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    private OnItemClickListener mylistener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mylistener = listener;
    }


    @Override
    public void onBindViewHolder(@NonNull Myholder holder, @SuppressLint("RecyclerView") int position) {
        HashMap<String,Object> dataItem=list1.get(position);
        String name=dataItem.get("name").toString();
        holder.textview1.setText(name);
        holder.textview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mylistener != null) {
                    mylistener.onItemClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list1.size();
    }

    public class Myholder extends RecyclerView.ViewHolder {
        TextView textview1;
        public Myholder(@NonNull View itemView) {
            super(itemView);
            textview1=itemView.findViewById(R.id.item1);

        }
    }

}
