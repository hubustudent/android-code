package com.example.work1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;



public class fragment2 extends Fragment{   //继承自Fragment类的Fragment3类。在该类中，重写了onCreateView方法用于创建视图
    private RecyclerView recyclerView;
    private List<String> list;
    private Context context;
    private Myadapter myadapter;

    @SuppressLint("MissingInflatedId")
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        String[] list1={"小明","小虎","小李","小张","小赵"};
        View view=inflater.inflate(R.layout.middle2,container,false);
        context=view.getContext();
        recyclerView=view.findViewById(R.id.recycleview1);
        list=new ArrayList();
        for(int i=0;i< list1.length;i++)
            list.add(list1[i]);

        myadapter = new Myadapter(context,list);
        myadapter.setOnItemClickListener(new Myadapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                String text = list.get(position);
                Intent intent = new Intent(getContext(), MainActivity1.class);
                intent.putExtra("data", text);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(myadapter);
        LinearLayoutManager manager=new LinearLayoutManager(context);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        return view;

    }
}