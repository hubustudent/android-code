package com.example.work1;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;



public class MainActivity1 extends AppCompatActivity {
    TextView textView;
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);
        String data= getIntent().getStringExtra("data");

        textView=findViewById(R.id.textView01);
        textView.setText(data);

        @SuppressLint("WrongViewCast")
        ImageView imageView=findViewById(R.id.imageView_e);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.putExtra("result","999");
                setResult(666,intent);
                finish();
            }
        });

    }
}